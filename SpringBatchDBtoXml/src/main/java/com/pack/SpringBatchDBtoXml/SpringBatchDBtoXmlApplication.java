package com.pack.SpringBatchDBtoXml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchDBtoXmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchDBtoXmlApplication.class, args);
	}

}
