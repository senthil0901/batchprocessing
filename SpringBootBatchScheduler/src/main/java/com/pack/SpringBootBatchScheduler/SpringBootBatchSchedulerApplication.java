package com.pack.SpringBootBatchScheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBatchSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBatchSchedulerApplication.class, args);
	}

}


//Here the job will be executed after every 5 seconds
//Inside target-classes-persons.csv