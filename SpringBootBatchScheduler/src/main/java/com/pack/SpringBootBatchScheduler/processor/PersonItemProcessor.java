package com.pack.SpringBootBatchScheduler.processor;

import org.springframework.batch.item.ItemProcessor;

import com.pack.SpringBootBatchScheduler.model.Person;



public class PersonItemProcessor implements ItemProcessor<Person, Person>{

	@Override
	public Person process(Person person) throws Exception {
		return person;
	}
}
