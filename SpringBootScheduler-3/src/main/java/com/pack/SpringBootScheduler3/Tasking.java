package com.pack.SpringBootScheduler3;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Tasking {
	
	//@Scheduled(fixedRate=5000) //every 5 sec task will be scheduled
	//@Scheduled(fixedRate=5000,initialDelay=5000) //first task started and after 5sec delay for each task
	@Scheduled(fixedDelay=5000) //delay for first and second run
	public void taskScheduling() {
		System.out.println("Welcome to task scheduling "+new java.util.Date());
	}

}
