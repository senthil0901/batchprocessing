package com.pack.SpringBootScheduler3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootScheduler3Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootScheduler3Application.class, args);
	}

}
