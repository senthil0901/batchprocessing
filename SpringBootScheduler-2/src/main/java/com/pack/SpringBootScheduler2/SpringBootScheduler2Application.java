package com.pack.SpringBootScheduler2;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class SpringBootScheduler2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootScheduler2Application.class, args);
	}

	@Scheduled(fixedDelay = 1000) //interval is 3sec,it waits for task to get completed and interval starts at end of execution(2 sec+1sec of delay)
	public void delayTask() throws InterruptedException {
        System.out.println(new Date().getSeconds());
        Thread.sleep(2000);
    }


	@Scheduled(fixedRate = 1000)//in first 1sec, it waits for task and immediately after execution of task next task will triggered
	//After 2 sec,each task is triggered
	public void fixedRateTask() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}

	@Scheduled(fixedDelay = 1000, initialDelay = 1000)
	public void delayTaskWithInitDelay() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}


	@Scheduled(fixedRate = 1000, initialDelay = 1000)
	public void fixedRateTaskInitDelay() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}


	@Scheduled(fixedDelayString = "${fixed.delay.millis}") //accepts parameter in string
	public void delayStringTask() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}

	@Scheduled(fixedDelayString = "${fixed.rate.millis}")
	public void dfixedRateStringTask() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}


	@Scheduled(fixedDelayString = "${fixed.delay.millis}", initialDelayString = "${init.delay.millis}")
	public void delayStringTaskWithInitDelay() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}

	@Scheduled(fixedDelayString = "${fixed.rate.millis}", initialDelayString = "${init.delay.millis}")
	public void dfixedRateStringTaskWithInitDelay() throws InterruptedException {
		System.out.println(new Date().getSeconds());
		Thread.sleep(2000);
	}
}
