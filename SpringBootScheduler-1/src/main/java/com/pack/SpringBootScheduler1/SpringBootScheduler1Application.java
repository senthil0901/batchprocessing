package com.pack.SpringBootScheduler1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootScheduler1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootScheduler1Application.class, args);
	}

}
