package com.pack.SpringBootScheduler1.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pack.SpringBootScheduler1.model.User;

public interface UserDao extends JpaRepository<User, Integer>{

}