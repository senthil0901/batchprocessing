package com.pack.MultiResourceItemReaderExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiResourceItemReaderExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiResourceItemReaderExampleApplication.class, args);
	}

}
